// Johnathan Burns and Ethan Spangler
// 2018-09-29
// Friendly Face face detection
//

// OpenCV includes
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

// AWS includes
#include <aws/core/Aws.h>
#include <aws/s3/S3Client.h>
#include <aws/s3/model/PutObjectRequest.h>

// cURLpp includes
#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Exception.hpp>
#include <curlpp/Infos.hpp>

// STL includes
#include <vector>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <time.h>
#include <chrono>
#include <thread>
#include <string>

using namespace std;
using namespace cv;

#define REQUIRED_DETECTIONS 5
#define OUT_VID_SIZE_W 160
#define OUT_VID_SIZE_H 120
#define SLEEP_MILLIS 4000
#define S3_BUCKET_NAME "friendlyface-bucket"
#define GET_URL "https://cwfkb6miil.execute-api.us-east-1.amazonaws.com/default/ffsearchimage?photo="

bool detectAndDisplay(Mat& frame, int currentDetectionLevel);
void uploadToS3Bucket(string fileName);
void makeGetRequest(string fileName);

// Gross global variables
String face_cascade_name = "haarcascade_frontalface_alt.xml";
CascadeClassifier face_cascade;
string window_name = "";
RNG rng(12121);

int main(int argc, char** argv)
{
	VideoCapture* capture;
	Mat frame;
	vector<int> fileOutputParams;
	char fileName[50];
	int prevDetect;
	time_t rawtime;
	struct tm * ptm;

	// Load the cascade
	if( !face_cascade.load(face_cascade_name))
	{
		printf("Error Loading\n");
		return -1;
	}

	printf("Loaded Cascade file\n");
	prevDetect = 0;

	// Read the video stream
	capture = new VideoCapture(-1);
	capture->set(CV_CAP_PROP_FRAME_WIDTH, OUT_VID_SIZE_W);
	capture->set(CV_CAP_PROP_FRAME_HEIGHT, OUT_VID_SIZE_H);

	// Set the file output parameters
	fileOutputParams.push_back(CV_IMWRITE_PNG_COMPRESSION);
	fileOutputParams.push_back(9);

	if(capture)
	{
		// Window properties only need set once
		namedWindow(window_name, WINDOW_NORMAL);
		setWindowProperty(window_name, CV_WND_PROP_FULLSCREEN, 1);

		while(true)
		{
			// Get the frame 
			capture->read(frame);

			// Apply the classifier to the frame
			if(!frame.empty())
			{
				if(prevDetect > REQUIRED_DETECTIONS)
				{
					// If a face is detected multiple frames in a row, then call the lambda function
					printf("Detected a face in %d consecutive frames, calling Lambda function.\n", REQUIRED_DETECTIONS);

					time(&rawtime);
					ptm = gmtime(&rawtime);
					sprintf(fileName, "Face-%02d-%02d-%02d.png", ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
					printf("File Name: %s\n", fileName);

					capture->read(frame);
					if(!frame.empty())
					{
						flip(frame, frame, 0);
					
						if(!imwrite(fileName, frame, fileOutputParams))
						{
							fprintf(stderr, "Unable to write image to %s.\n", fileName);
						}
						else
						{
							uploadToS3Bucket(fileName);
							makeGetRequest(fileName);
						}
					}
					else
					{
						fprintf(stderr, "Could not write to file, frame was empty.\n");
					}

					prevDetect = 0;

					// Give the picture taking some hysterisis
//					this_thread::sleep_for(chrono::milliseconds(SLEEP_MILLIS));
				}
				else
				{
					if(detectAndDisplay(frame, prevDetect)) prevDetect++;
					else prevDetect = 0;
				}
			}
			else
			{
				printf("No frame captured\n");
				break;
			}

			int c = waitKey(10);
			if( (char)c == 'c') break;
		}
	}
	return 0;
}

bool detectAndDisplay(Mat& frame, int currentDetectionLevel)
{
	// Local variables
	vector<Rect> faces;
	Mat frame_grey;

	// Flip the image vertically before processing
	flip(frame, frame, 0);

	// Transform the image from the frame into a greyscale image
	cvtColor(frame, frame_grey, CV_BGR2GRAY);
	equalizeHist(frame_grey, frame_grey);

	// Detect faces
	face_cascade.detectMultiScale(frame_grey, faces, 1.3, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30,30));
	for(size_t i = 0; i < faces.size(); i++)
	{
		// Draw a circle around the detected faces
		//Point center(faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5)
		Rect face_rect(faces[i].x, faces[i].y, faces[i].width, faces[i].height);
		rectangle(frame, face_rect, Scalar(0, (int)(55 + (currentDetectionLevel * 200 / REQUIRED_DETECTIONS)), 0));
		//ellipse(frame, center, Size(faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, Scalar(255, 0, 255), 4, 8, 0);
	}

	transpose(frame, frame);
	flip(frame, frame, +1);
	flip(frame, frame, 0);
	imshow(window_name, frame);

	return (faces.size() > 0);
}



void uploadToS3Bucket(string fileName)
{
	Aws::SDKOptions options;
	Aws::InitAPI(options);
	printf("Starting with file %s\n", fileName.c_str());
	Aws::Client::ClientConfiguration clientConfig;
	Aws::S3::S3Client s3Client(clientConfig);

	printf("Creating objectRequest\n");
	Aws::S3::Model::PutObjectRequest objectRequest;
	objectRequest.WithBucket(S3_BUCKET_NAME).WithKey(fileName.c_str());

	printf("Creating inputData\n");
	shared_ptr<fstream> inputData = make_shared<fstream>(fileName, ios_base::in | ios_base::binary | ios_base::ate);

	objectRequest.SetContentLength(inputData->tellg());
	objectRequest.SetBody(inputData);

	printf("Getting outcome\n");
	auto putObjectOutcome = s3Client.PutObject(objectRequest);

	if(putObjectOutcome.IsSuccess())
	{
		printf("Uploaded %s to S3 bucket!\n", fileName.c_str());
	}
	else
	{
		fprintf(stderr, "Unable to upload %s to S3 bucket.\nError: %s %s", fileName.c_str(), putObjectOutcome.GetError().GetExceptionName().c_str(), putObjectOutcome.GetError().GetMessage().c_str());
	}

	Aws::ShutdownAPI(options);
}

void makeGetRequest(string fileName)
{
	string finalUrl = GET_URL + fileName;

	printf("Attempting connection to %s\n", finalUrl.c_str());
	try
	{
		curlpp::Cleanup cleaner;
		curlpp::Easy request;

		request.setOpt(curlpp::Options::Verbose(false));
		request.setOpt(curlpp::Options::Url(finalUrl));

		request.perform();
		
		string effURL;
		curlpp::infos::EffectiveUrl::get(request, effURL);
		printf("Effective URL: %s\n", effURL.c_str());

	}
	catch(curlpp::LogicError & e)
	{
		fprintf(stderr, "%s\n", e.what());
	}
	catch(curlpp::RuntimeError & e)
	{
		fprintf(stderr, "%s\n", e.what());
	}
}
